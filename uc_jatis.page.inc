<?php

/**
 * @file
 * Provides Jatis Request Response 
 */
/**
 * Respond code definition.
 * See VA Merchant Integration API 1.7
 */
define('UC_JATIS_PAYMENT_APPROVED', '00'); // Approve or Completed successfully 
define('UC_JATIS_PAYMENT_ERROR', '06'); // Error, error code that is not on the list
define('UC_JATIS_PAYMENT_INVALID_TRANSACTION', '12'); // Invalid transaction flow ( payment without inquery )
define('UC_JATIS_PAYMENT_INVALID_AMOUNT', '13'); // Invalid amount
define('UC_JATIS_PAYMENT_VA_NOTFOUND', '14'); // VA / ID not found
define('UC_JATIS_PAYMENT_REVERSAL_NOTFOUND', '25'); // Reversal fail ( original transaction not found )
define('UC_JATIS_PAYMENT_PENDING', '99'); // Payment Pending

/**
 * Page callback for Inquery Request Response 
 */
function uc_jatis_atm_page_inquiry() {

  // Send request to Access Denied Page for invalid request      
  $request = drupal_get_query_parameters();
  $valid_request = uc_jatis_validate_request('inquiry', $request, $_SERVER['REMOTE_ADDR']);
  $valid_hash = uc_jatis_validate_hash('inquiry', $request);

  // continue process request if hash is valid and send error instead
  if ($valid_hash) {
    // create response array  
    if ($valid_request) {
      // get record for pending transaction
      $result = db_select('uc_payment_jatis_atm', 'j')
          ->fields('j')
          ->condition('j.vanumber', $request['vanumber'], '=')
          ->condition('j.status', '99', '=')
          ->execute()
          ->fetchAssoc();
      // get order info
      $orders = uc_order_load($result['order_id']);
      // generate full name
      if ($orders->delivery_first_name != '') {
        $name = trim($orders->delivery_first_name . ' ' . $orders->delivery_last_name);
      }
      else {
        $name = trim($orders->billing_first_name . ' ' . $orders->billing_last_name);
      }
      // generate txref
      $trxref = '1';
      for ($i = 0; $i < (7 - strlen($orders->order_id)); $i++) {
        $trxref .= '0';
      }
      $trxref .= $orders->order_id;

      // send response transaction successfully or transaction not found
      if ($result && $orders) {
        // create response array
        $response = array(
          $request['vanumber'],
          UC_JATIS_PAYMENT_APPROVED,
          $request['stan'],
          $name,
          $orders->order_total,
          $trxref,
        );
        // update txref to database
        db_update('uc_payment_jatis_atm')
            ->fields(array(
              'trxref' => $trxref,
            ))
            ->condition('vanumber', $request['vanumber'], '=')
            ->execute();
        // update order to processing
        uc_order_update_status($orders->order_id, 'processing');
        // add comment to order
        uc_order_comment_save($orders->order_id, 0, $orders->payment_method . ' processing', 'admin');
      }
      else {
        // create response array
        if (!$result) {
          $response = array(
            $request['vanumber'],
            UC_JATIS_PAYMENT_VA_NOTFOUND,
            $request['stan'],
            'VA/ID Not Found',
          );
        }
        elseif (!$orders) {
          $response = array(
            $request['vanumber'],
            UC_JATIS_PAYMENT_ERROR,
            $request['stan'],
            'Order ID Not Found',
          );
        }
      }
      print implode('|', $response);
    }
    else {
      drupal_access_denied();
    }
  }
  else {
    $response = array(
      $request['vanumber'],
      UC_JATIS_PAYMENT_ERROR,
      $request['stan'],
      'Invalid Hash',
    );
    print implode('|', $response);
  }
  // log inquiry
  watchdog('uc_jatis', 'Inquiry message: %msg ', array('%msg' => implode('|', $response)), WATCHDOG_NOTICE, $link = NULL);
}

/**
 * Page callback for Payment Confirmation Request Response
 */
function uc_jatis_atm_page_confirmation() {
  // Send request to Access Denied Page for invalid request      
  $request = drupal_get_query_parameters();
  $valid_request = uc_jatis_validate_request('confirmation', $request, $_SERVER['REMOTE_ADDR']);
  $valid_hash = uc_jatis_validate_hash('confirmation', $request);
  // continue process request if hash is valid and send error instead
  if ($valid_hash) {
    // create response array  
    if ($valid_request) {
      // get record for pending transaction
      $result = db_select('uc_payment_jatis_atm', 'j')
          ->fields('j')
          ->condition('j.vanumber', $request['vanumber'], '=')
          ->condition('j.status', '99', '=')
          ->execute()
          ->fetchAssoc();
      // get order info
      $orders = uc_order_load($result['order_id']);
      // send response transaction successfully or transaction not found
      if ($result && $orders) {
        // create response array        
        if ($orders->order_total == $request['amount']) {
          $response = array(
            $request['vanumber'],
            UC_JATIS_PAYMENT_APPROVED,
            $request['stan'],
            'Transaction Success',
          );
          // update txref to database
          db_update('uc_payment_jatis_atm')
              ->fields(array(
                'status' => UC_JATIS_PAYMENT_APPROVED,
              ))
              ->condition('vanumber', $request['vanumber'], '=')
              ->execute();
          // update order to payment received
          uc_order_update_status($orders->order_id, 'payment_received');
          uc_order_comment_save($orders->order_id, 0, $orders->payment_method . ' received', 'admin');
        }
        else {
          $response = array(
            $request['vanumber'],
            UC_JATIS_PAYMENT_ERROR,
            $request['stan'],
            'Invalid Amount',
          );
        }
      }
      else {
        // create response array
        if (!$result) {
          $response = array(
            $request['vanumber'],
            UC_JATIS_PAYMENT_VA_NOTFOUND,
            $request['stan'],
            'VA/ID Not Found',
          );
        }
        elseif (!$orders) {
          $response = array(
            $request['vanumber'],
            UC_JATIS_PAYMENT_ERROR,
            $request['stan'],
            'Order ID Not Found',
          );
        }
      }
      print implode('|', $response);
    }
    else {
      drupal_access_denied();
    }
  }
  else {
    $response = array(
      $request['vanumber'],
      UC_JATIS_PAYMENT_ERROR,
      $request['stan'],
      'Invalid Hash',
    );
    print implode('|', $response);
  }
  // log confirmation
  watchdog('uc_jatis', 'Confirmation message: %msg ', array('%msg' => implode('|', $response)), WATCHDOG_NOTICE, $link = NULL);
}

/**
 * Page callback for Payment Reversal Request Response
 */
function uc_jatis_atm_page_reversal() {
  // Send request to Access Denied Page for invalid request      
  $request = drupal_get_query_parameters();
  $valid_request = uc_jatis_validate_request('reversal', $request, $_SERVER['REMOTE_ADDR']);
  $valid_hash = uc_jatis_validate_hash('reversal', $request);
  // continue process request if hash is valid and send error instead
  if ($valid_hash) {
    // create response array  
    if ($valid_request) {
      // get record for pending transaction
      $result = db_select('uc_payment_jatis_atm', 'j')
          ->fields('j')
          ->condition('j.vanumber', $request['vanumber'], '=')
          ->condition('j.status', '00', '=')
          ->execute()
          ->fetchAssoc();
      // get order info
      $orders = uc_order_load($result['order_id']);
      // send response transaction successfully or transaction not found      
      if ($result && $orders) {
        // create response array        
        if ($orders->order_total == $request['amount']) {
          $response = array(
            $request['vanumber'],
            UC_JATIS_PAYMENT_APPROVED,
            $request['stan'],
            'Reversal Success',
          );
          // update reversal status to database
          db_update('uc_payment_jatis_atm')
              ->fields(array(
                'status' => UC_JATIS_PAYMENT_PENDING,
                'reversal' => '1',
              ))
              ->condition('vanumber', $request['vanumber'], '=')
              ->execute();
          // update order to payment pending
          uc_order_update_status($orders->order_id, 'pending');
          uc_order_comment_save($orders->order_id, 0, $orders->payment_method . ' reversal received', 'admin');
        }
        else {
          $response = array(
            $request['vanumber'],
            UC_JATIS_PAYMENT_ERROR,
            $request['stan'],
            'Invalid Amount',
          );
        }
      }
      else {
        // create response array
        if (!$result) {
          $response = array(
            $request['vanumber'],
            UC_JATIS_PAYMENT_VA_NOTFOUND,
            $request['stan'],
            'VA/ID Not Found',
          );
        }
        elseif (!$orders) {
          $response = array(
            $request['vanumber'],
            UC_JATIS_PAYMENT_ERROR,
            $request['stan'],
            'Order ID Not Found',
          );
        }
      }
      print implode('|', $response);
    }
    else {
      drupal_access_denied();
    }
  }
  else {
    $response = array(
      $request['vanumber'],
      UC_JATIS_PAYMENT_ERROR,
      $request['stan'],
      'Invalid Hash',
    );
    print implode('|', $response);
  }
  // log reversal
  watchdog('uc_jatis', 'Reversal message: %msg ', array('%msg' => implode('|', $response)), WATCHDOG_NOTICE, $link = NULL);
}

/**
 * Validate incoming request with listed IP Address 
 * Make sure all request coming from valid Jatis Server
 * @param $op 
 *  Request operation available paramater: "inquiry","confirmation" and "reversal"
 * @param $request
 *  Raw Request from Jatis Server     
 * @param $remote_address
 *  Remote IP Address
 * @return
 *  Return valid or invalid request
 */
function uc_jatis_validate_request($op, $request, $remote_address) {
  // log request
  watchdog('uc_jatis', 'Incoming: %op From : %remote ', array('%op' => $op, '%remote' => $remote_address), WATCHDOG_NOTICE, $link = NULL);
  // validate remote address with whitelisted IP  
  $ip_validated = FALSE;
  if (variable_get('uc_jatis_enable_ip_validation') == 1) {
    $valid_ip_addresses = explode(',', variable_get('uc_jatis_allowed_ip_addresses'));
    if (in_array($remote_address, $valid_ip_addresses)) {
      $ip_validated = TRUE;
    }
  }

  // validate request format
  $request_validated = FALSE;

  // array definition for each valid request
  $inquiry_request_format = array('hash', 'vanumber', 'stan', 'trxdate');
  $confirmation_request_format = array('hash', 'vanumber', 'stan', 'amount', 'trxdate', 'trxref');
  $reversal_request_format = array('hash', 'vanumber', 'stan', 'amount', 'trxdate');
  // key array for request
  $request_key = array_keys($request);

  switch ($op) {
    case 'inquiry':
      $request_key_diff = array_diff($inquiry_request_format, $request_key);
      if (count($request_key_diff) == 0) {
        $request_validated = TRUE;
      }
      break;
    case 'confirmation':
      $request_key_diff = array_diff($confirmation_request_format, $request_key);
      if (count($request_key_diff) == 0) {
        $request_validated = TRUE;
      }
      break;
    case 'reversal':
      $request_key_diff = array_diff($reversal_request_format, $request_key);
      if (count($request_key_diff) == 0) {
        $request_validated = TRUE;
      }
      break;
  }

  // return validated request
  if (variable_get('uc_jatis_enable_ip_validation') == 1) {
    if ($ip_validated == TRUE && $request_validated == TRUE) {
      return TRUE;
    }
    elseif ($request_validated == TRUE) {
      return TRUE;
    }
    else {
      return FALSE;
    }
  }
  else {
    return $request_validated;
  }
}

/**
 * Generate hash number
 * @param $op 
 *  Request operation available paramater: "inquiry","confirmation" and "reversal"
 * @param $request
 *  Raw Request from Jatis Server     
 * @return 
 *  Return true for valid hash
 */
function uc_jatis_validate_hash($op, $request) {
  $valid_hash = FALSE;
  // salt = digit 5,6 and 15,16 mod 7  
  $salt = (substr($request['vanumber'], 4, 2) . substr($request['vanumber'], 14, 2));
  $salt = $salt . ($salt % 7);

  switch ($op) {
    case 'inquiry':
      $hash_format = 'vanumber=' . $request['vanumber'] . '&stan=' . $request['stan'] . '&trxdate=' . $request['trxdate'] . '&' . $salt;
      break;
    case 'confirmation':
      $hash_format = 'vanumber=' . $request['vanumber'] . '&stan=' . $request['stan'] . '&amount=' . $request['amount'] . '&trxdate=' . $request['trxdate'] . '&trxref=' . $request['trxref'] . '&' . $salt;
      break;
    case 'reversal':
      $hash_format = 'vanumber=' . $request['vanumber'] . ' &stan=' . $request['stan'] . '&amount=' . $request['amount'] . '&trxdate=' . $request['trxdate'] . '&' . $salt;
      break;
  }

  // log validation message for easy debugging    
  watchdog('uc_jatis', '%op hash format: %format ', array('%op' => $op, '%format' => strtoupper($hash_format)), WATCHDOG_NOTICE, $link = NULL);
  watchdog('uc_jatis', '%op hash: %hash ', array('%op' => $op, '%hash' => md5(strtoupper($hash_format))), WATCHDOG_NOTICE, $link = NULL);

  // validate hash
  if ($request['hash'] == md5(strtoupper($hash_format))) {
    $valid_hash = TRUE;
  }

  return $valid_hash;
}

/**
 * Page callback for Credit Card Request Response
 */
function uc_jatis_cc_page_redirect() {
  // get credit card payment responds and parameters    
  $merchant_tranid = $_POST['MERCHANT_TRANID'];
  $txn_status = $_POST['TXN_STATUS'];
  $output = '';
  // set data session for Order ID
  $_SESSION['order_data'] = $merchant_tranid;

  watchdog('uc_jatis', '%req', array('%req' => serialize($_POST)));

  if ($txn_status == 'S' && $merchant_tranid != '') {
    // update order to payment received
    uc_order_update_status($merchant_tranid, 'payment_received');
    uc_order_comment_save($merchant_tranid, 0, 'Credit Card Payment received', 'admin');
    $output .= '<h2 class="trans-success">' . t('Payment Success') . '</h2>';
  }
  elseif ($merchant_tranid != '') {
    // update order to payment received
    uc_order_update_status($merchant_tranid, 'payment_pending');
    uc_order_comment_save($merchant_tranid, 0, 'Credit Card Payment pending', 'admin');
    $output .= '<h2 class="trans-failed">' . t('Payment Failed') . '</h2>';
  }
  else {
    $output .= '<h2 class="trans-failed">' . t('Payment Failed. Unknown Order ID') . '</h2>';
  }
  return $output;
}
