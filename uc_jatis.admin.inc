<?php

/**
 * @file
 * Jatis Paymeny Gateway Configuration
 */
function uc_jatis_admin_settings_form($form, &$form_state) {

  // Immediate Payment Form
  $form['immediate_payment'] = array(
    '#type' => 'fieldset',
    '#title' => t('Immediate Payment Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['immediate_payment']['uc_jatis_cc_merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t('Jatis IP Merchant ID'),
    '#default_value' => variable_get('uc_jatis_cc_merchant_id', ''),
  );

  $form['immediate_payment']['uc_jatis_cc_password'] = array(
    '#type' => 'password',
    '#title' => t('Password'),
    '#size' => 50,
    '#maxlength' => 255,
    '#description' => t('Jatis IP Password'),
    '#attributes' => array('value' => variable_get('uc_jatis_cc_password', '')),
  );

  $form['immediate_payment']['uc_jatis_cc_return_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Return URL'),
    '#size' => 75,
    '#maxlength' => 255,
    '#description' => t('Return URL'),
    '#attributes' => array('value' => variable_get('uc_jatis_cc_return_url', '')),
  );

  // Deferred Payment Form
  $form['deferred_payment'] = array(
    '#type' => 'fieldset',
    '#title' => t('Deferred Payment Configuration'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['deferred_payment']['uc_jatis_va_merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#size' => 6,
    '#maxlength' => 6,
    '#description' => t('Jatis DP Merchant ID/VA Number (6 digit number)'),
    '#default_value' => variable_get('uc_jatis_va_merchant_id', ''),
  );

  $form['deferred_payment']['uc_jatis_va_order_expired'] = array(
    '#type' => 'textfield',
    '#title' => t('Order Expired In'),
    '#size' => 6,
    '#maxlength' => 2,
    '#description' => t('Order expiration in hours'),
    '#default_value' => variable_get('uc_jatis_va_order_expired', ''),
  );

  $form['deferred_payment']['uc_jatis_va_enable_expired_order'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable order expiration date'),
    '#description' => t('Enable order expiration date for payment'),
    '#default_value' => variable_get('uc_jatis_va_enable_expired_order', '1'),
  );

  // Jatis IP Address
  $form['ip_address'] = array(
    '#type' => 'fieldset',
    '#title' => t('Jatis Allowed IP Address'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['ip_address']['uc_jatis_allowed_ip_addresses'] = array(
    '#type' => 'textarea',
    '#title' => t('Jatis Allowed IP Address'),
    '#description' => t('Allowed Jatis IP Address. Used to validate incoming request. Multiple value separated by comma.'),
    '#default_value' => variable_get('uc_jatis_allowed_ip_addresses', ''),
  );

  $form['ip_address']['uc_jatis_enable_ip_validation'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable IP Address Validation'),
    '#description' => t('Enable IP Address validation for Jatis Server'),
    '#default_value' => variable_get('uc_jatis_enable_ip_validation', '1'),
  );

  // jatis installment options
  $form['installment'] = array(
    '#type' => 'fieldset',
    '#title' => t('Jatis Installment Options'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $form['installment']['uc_jatis_installment_options'] = array(
    '#type' => 'textarea',
    '#title' => t('Jatis Installment Options'),
    '#description' => t('Enter installment options per line. Dont use ( ) characters.'),
    '#default_value' => variable_get('uc_jatis_installment_options', '; example' . PHP_EOL . '; Permata = 3,6,12 '),
  );

  $form['installment']['uc_jatis_enable_installment_options'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable Installment Options'),
    '#description' => t('Enable Installment options for credit card.'),
    '#default_value' => variable_get('uc_jatis_enable_installment_options', '1'),
  );

  return system_settings_form($form);
}

function uc_jatis_admin_settings_form_validate($form, &$form_state) {
  if ($form_state['values']['uc_jatis_cc_password'] == '') {
    form_set_error('uc_jatis_cc_password', t('Please provide password!'));
  }

  if (!is_numeric($form_state['values']['uc_jatis_va_order_expired'])) {
    form_set_error('uc_jatis_cc_password', t('Enter numeric value!'));
  }
}
